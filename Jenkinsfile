import groovy.transform.Field

@Field final String APP_NAME = 'car_configurator_system'
@Field final String CREDENTIALS_ID = '1a620f07-dac8-4338-8a34-0ce37d6c3e24'
@Field final String SONARQUBE_TOKEN = 'c4c43a5c4561490a832425cc8c8ea9cdb680a505'
@Field final String SONARQUBE_URL = 'https://sonarcloud.io/dashboard?id=car_configuration_system'

@Field final String ORG_NAME = 'Adesso'
@Field final String SPACE_NAME = 'AdessoWebApp'
@Field final String SPACE_NAME_JENKINS_TEST = 'QA'


@Field final String MAVEN_VERSION = 'maven_3_6_0'


node('windows'){

//*************************//
//********* BUILD *********//
//*************************//
    stage('build') {
        node('windows') {
            try {
                // killZombies()
                deleteDir() /* clean up our workspace */

                checkout scm
                stash name: 'workspace', includes: '**, .git/', useDefaultExcludes: false
                stash name: 'manifest', includes: 'manifest.yml'

                def branchName = env.BRANCH_NAME
                currentBuild.displayName = "#${env.BUILD_ID} - branch: ${branchName}"

                withEnv( ["PATH+MAVEN=${tool MAVEN_VERSION}/bin"] ) {
                    bat 'mvn -Dmaven.test.skip=true clean install'
                }
                currentBuild.result = 'SUCCESS'
            }
            catch(Exception error) {
                currentBuild.result = 'FAILURE'
                throw error
            }
            finally {
                deleteDir()
            }
        }
    }


//*************************//
//******* UNIT TEST *******//
//*************************//
// stage('unit test') {

    stage('unit test') {
        node('windows') {
            try {
                deleteDir() /* clean up our workspace */

                checkout scm

                def branchName = env.BRANCH_NAME
                currentBuild.displayName = "#${env.BUILD_ID} - branch: ${branchName}"

                withEnv( ["PATH+MAVEN=${tool MAVEN_VERSION}/bin"] ) {
                    bat 'mvn test'
                }
                currentBuild.result = 'SUCCESS'
            }
            catch(Exception error) {
                currentBuild.result = 'FAILURE'
                throw error
            }
            finally {
                deleteDir()
            }
        }
    }

//*************************//
//******* SONAR QUBE *******//
//*************************//

    stage('sonar qube') {
        node('windows') {
            try {
                deleteDir() /* clean up our workspace */

                checkout scm

                def branchName = env.BRANCH_NAME
                currentBuild.displayName = "#${env.BUILD_ID} - branch: ${branchName}"
                withEnv( ["PATH+MAVEN=${tool MAVEN_VERSION}/bin"] ) {
                    bat 'mvn sonar:sonar -Dsonar.login=c4c43a5c4561490a832425cc8c8ea9cdb680a505 -Dsonar.host.url=https://sonarcloud.io' +
                            ' -Dsonar.organization=trsuix-github -Dsonar.projectKey=car_configuration_system'
                }
                currentBuild.result = 'SUCCESS'
            }
            catch(Exception error) {
                currentBuild.result = 'FAILURE'
                throw error
            }
            finally {
                deleteDir()
            }
        }
    }



//*************************//
//****** SONAR TEST *******//
//*************************//
// stage('sonar test') {
//     node('Windows') {
//         gitlabCommitStatus("test:sonar") {
//             try {
//                 killZombies()
//                 deleteDir()

//                 unstash 'workspace'

//                 def nodejs = tool NODE_JS_VERSION
//                 withEnv(["PATH+NODEJS=${nodejs}/bin"]) {
//                     def sonarScanner = tool('SonarQube-Scanner') +'/bin/sonar-scanner'
//                     withSonarQubeEnv('sonar.bk.datev.de') {
//                         unstash 'jasmine-unit-reports.xml'
//                         unstash 'protractor-unit-reports.xml'
//                         sh "${sonarScanner} -Dsonar.branch=${BRANCH_NAME}"
//                     }
//                 }
//             }
//             catch(Exception error) {
//                 currentBuild.result = 'FAILURE'
//                 throw error
//             }
//             finally {
//                 killZombies()
//                 deleteDir()
//             }
//         }
//     }
// }




//*************************//
//******** CF PUSH ********//
//*************************//
//milestone 3
    stage('cf push') {
        timeout time: 24, unit: 'HOURS', message: 'Do you want to push the app to Pivotal Cloud Foundry (PaaS)?', continueAfterAbort: true, showTimeoutInMessage: true, {
            node('windows') {

                try {
                    deleteDir()

                    unstash 'workspace_npm-build'
                    unstash 'manifest'

                    def cfPath = tool('cf-cli')
                    withEnv(["PATH+CF=${cfPath}"]) {
                        withCredentials([usernamePassword(credentialsId: CREDENTIALS_ID, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                            bat 'cf-login -a https://api.run.pivotal.io -o ' + ORG_NAME + ' -s ' + SPACE_NAME
                            try {
                                bat 'cf push ' + APP_NAME
                                currentBuild.result = 'SUCCESS'
                            } catch (err) {
                                echo "Failed: ${err}"
                                currentBuild.result = 'FAILURE'
                            } finally {
                                bat 'cf logout'
                            }
                        }
                    }

                    def branchName = env.BRANCH_NAME
                    currentBuild.displayName = "#${BUILD_NUMBER} - branch: ${branchName} --> CF"
                    currentBuild.result = 'SUCCESS'
                }
                catch (Exception error) {
                    currentBuild.result = 'FAILURE'
                    throw error
                }
                finally {
                    deleteDir()
                    sendMail()
                }
            }
        }
    }


//*************************//
//******** OPENSHIFT PUSH ********//
//*************************//
    stage('openshift push') {

        node('windows') {
            stage 'buildInDevelopment'
            openshiftBuild(namespace: 'dev', buildConfig: 'car_configurator_system', showBuildLogs: 'true')
            stage 'deployInDevelopment'
            openshiftDeploy(namespace: 'dev', deploymentConfig: 'car_configurator_system')
            openshiftScale(namespace: 'dev', deploymentConfig: 'car_configurator_system',replicaCount: '2')
        }
    }



}

def sendMail(){
    mail (to: 'car.configurator.system@gmail.com',
            subject: "Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) - ${currentBuild.result}",
            body: "Please go to ${env.BUILD_URL}.");
}
